# Boring ZFS Backup

BZB is a very simple tool to create, list and restore backups from Google Cloud Storage.

### Configuration

The configuration happens in `/etc/boring-zfs-backup.conf`.  
You can find a sample configuration under `sample.conf`

You will need to be logged in to gcloud via a service account, that can create objects in your designated bucket.
You will need to have set up a GPG key to encrypt to (paste the fingerprint into the config). The private key will be needed when restoring from a remote location.

### Dependencies

The following commandline tools have to be installed (and configured where applicable)

 - zfs
 - pv
 - gpg
 - split
 - gsutil
 - date

### State

Current state:

Implemented:
 - Listing existing remote backups
 - Backing up to remote GCP bucket with encryption
 - Restoring local snapshots to a clone

ToDo:
 - Restore backups from a remote GCP bucket and decryption
 - In-app help text
 - Include local snapshots in the backup list and mark snapshots are remote and/or local
