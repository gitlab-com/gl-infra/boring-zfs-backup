#!/usr/bin/env bash

set -eufo pipefail

source /etc/boring-zfs-backup.conf

chunksize=$((${chunksize_mib} * 1024 * 1024))

check_tools () {
  hash zfs
  hash pv
  hash gpg
  hash split
  hash gsutil
  hash date
  hash awk
  hash sed
}

start_backup () {

  if ! gsutil ls "${bucket}" &> /dev/null; then
    echo "Please ensure gsutil is configured and the bucket is correct" 2>&1
    exit 1
  fi

  local snapshot=$(date --iso-8601=s | sed 's;+.*;;g')
  local dataset="$1"
  local dataset_safe=$(sed 's;/;-;g' <<< $dataset)
  local prefix="${hostname}_${dataset_safe}_${snapshot}"

  sudo zfs snapshot "${dataset}@${snapshot}"
  sudo zfs send -cpP "${dataset}@${snapshot}" | pv | split -d -a 5 -b ${chunksize} --filter "gpg -r ${gpg_target} --cipher-algo AES256 --compress-algo none --encrypt > \$FILE.gpg && echo && gsutil cp \$FILE.gpg ${bucket}/${hostname}/${dataset_safe}/${snapshot}/ && rm \$FILE.gpg" - part_
  touch ok
  gsutil cp "ok" "${bucket}/${hostname}/${dataset_safe}/${snapshot}/"
  rm "ok"

}

list_backups () {
  echo "Available backups for '${hostname}'"
  echo

  for dataset in $(gsutil ls "${bucket}/${hostname}" | sed "s?${bucket}/${hostname}/??g; s?/??g"); do
    echo "> dataset '$dataset'"
    for snapshot in $(gsutil ls "${bucket}/${hostname}/${dataset}" | sed "s?${bucket}/${hostname}/${dataset}/??g; s?/??g" | sort -V); do
      if gsutil ls "${bucket}/${hostname}/${dataset}/${snapshot}/ok" &>/dev/null; then
        echo ">> ${snapshot}"
      else
        echo ">> ${snapshot} - incomplete"
      fi
    done
  done
}

restore_backup () {
  local dataset="$1"
  local snapshot="$2"
  local target="$3"

  for local_snapshot in $(zfs list -t snapshot -H ${dataset} | awk '{split($1, arr, "@"); print arr[2]}'); do
    if [ "${local_snapshot}" = "${snapshot}" ]; then
      restore_local "${dataset}" "${snapshot}" "${target}"
      exit 0
    fi
  done

  restore_remote  "${dataset}" "${snapshot}" "${target}"
}

restore_local () {
  local dataset="$1"
  local snapshot="$2"
  local dataset_safe=$(sed 's;/;-;g' <<< $dataset)
  local target="$3"

  echo "Snapshot '${snapshot}' available locally."

  sudo zfs clone -o "mountpoint=/${target}" "${dataset}@${snapshot}" "${target}"
  sudo zfs list ${target}
}

restore_remote () {
  local dataset="$1"
  local snapshot="$2"
  local dataset_safe=$(sed 's;/;-;g' <<< $dataset)
  local target="$3"

  if ! gsutil ls "${bucket}/${hostname}/${dataset_safe}/${snapshot}/ok" &>/dev/null; then
    echo "Snapshot '${snapshot}' not available locally, nor on remote. (Might be incomplete?)"
    exit 1
  fi

  echo "Snapshot '${snapshot}' available on remote."

################################################## TODO ###################################################
}

help () {
echo "TODO"
}

echo ' ______  ____________  ' 1>&2
echo ' | ___ \|___  /| ___ \ ' 1>&2
echo ' | |_/ /   / / | |_/ / ' 1>&2
echo ' | ___ \  / /  | ___ \ ' 1>&2
echo ' | |_/ /./ /___| |_/ / ' 1>&2
echo ' \____/ \_____/\____/  ' 1>&2
echo 1>&2
echo ' > Boring ZFS Backup < ' 1>&2
echo 1>&2

check_tools >/dev/null

case "$1" in
  backup)
	test -n "$2"
	start_backup "$2"
	;;
  list)
	list_backups
	;;
  restore)
	restore_backup "$2" "$3" "$4"
	;;
  *help)
	help
	;;
  *)
	echo "Unsupported subcommand '$1'" 1>&2
        help
	exit 1
	;;
esac

